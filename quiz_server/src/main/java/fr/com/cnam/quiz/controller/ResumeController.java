package fr.com.cnam.quiz.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.model.Resume;
import fr.com.cnam.quiz.model.entities.Answer;
import fr.com.cnam.quiz.model.entities.Question;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
@RestController
public class ResumeController {
    public ResumeController() {

    }

    @GetMapping("/resume")
    public Resume onQuizEnd(){
        Resume resume = QuizManager.getInstance().getResume();
        List<Question> questions = resume.getQuestions();

        if (resume != null && questions != null){
            StringBuilder sb = new StringBuilder();
            for (Question question : questions) {
                sb.append(question.toString()).append("\n");
                for (Answer answer : question.getAnswers()) {
                    sb.append("\t").append(answer.toString());
                }
                sb.append("\n\n");
            }
            System.out.println(sb.toString());
        }
        return resume;
    }
}
