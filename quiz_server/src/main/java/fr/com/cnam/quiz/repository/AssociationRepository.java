package fr.com.cnam.quiz.repository;

import fr.com.cnam.quiz.model.entities.Association;
import fr.com.cnam.quiz.model.entities.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociationRepository extends CrudRepository<Association, Integer> {
    Iterable<Association> findAllByQuestion(Question question);
}
