package fr.com.cnam.quiz.controller;

import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.repository.AnswerRepository;
import fr.com.cnam.quiz.repository.AssociationRepository;
import fr.com.cnam.quiz.repository.ModuleRepository;
import fr.com.cnam.quiz.repository.QuestionRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class ModuleController {
    public ModuleController(ModuleRepository repository, AssociationRepository associationRepository, AnswerRepository answerRepository, QuestionRepository questionRepository) {
        this.repository = repository;
        this.associationRepository = associationRepository;
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
        QuizManager.getInstance().setAssociationRepository(associationRepository);
        QuizManager.getInstance().setQuestionRepository(questionRepository);
        QuizManager.getInstance().setAnswerRepository(answerRepository);
    }

    private final ModuleRepository repository;
    private final AssociationRepository associationRepository;
    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;

    @GetMapping("/module")
    public Iterable<Module> onGetAll(){
        return repository.findAllByTheme(QuizManager.getInstance().getTheme());
    }

    @GetMapping("/module/{moduleId}")
    public RedirectView onSelected(@PathVariable Integer moduleId){
        Module module = repository.findById(moduleId).orElseThrow(() -> new NullPointerException("Configuration not found for this id :: " + moduleId));
        QuizManager.getInstance().setModule(module);
        System.out.println(QuizManager.getInstance().getModule().toString());
        return new RedirectView("/question");
    }
}
