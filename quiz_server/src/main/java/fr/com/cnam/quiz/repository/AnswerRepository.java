package fr.com.cnam.quiz.repository;

import fr.com.cnam.quiz.model.entities.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, Integer> {

}
