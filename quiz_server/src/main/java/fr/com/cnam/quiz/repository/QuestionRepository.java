package fr.com.cnam.quiz.repository;

import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.model.entities.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Integer> {
    Iterable<Question> findQuestionsByModule(Module module);
}
