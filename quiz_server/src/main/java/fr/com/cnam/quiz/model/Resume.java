package fr.com.cnam.quiz.model;

import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.model.entities.Question;

import java.util.ArrayList;
import java.util.List;

public class Resume {
  private List<Question> questions;
  private Integer[] userAnswers;
  private Boolean[] checkedValidAnswers;
  private Integer iterator;

  public Boolean[] getCheckedValidAnswers() { return checkedValidAnswers; }
  public Integer[] getUserAnswers() { return userAnswers; }
  public List<Question> getQuestions() { return questions; }

  public Question getCurrentQuestion() {
    return (questions.size() > 0) ? questions.get(iterator) : null;
  }

  public void goToNextQuestion(){
    iterator++;
  }

  public Integer getCurrentUserAnswer() {
    return (userAnswers.length > 0) ? userAnswers[iterator] : null;
  }

  public Boolean getCurrentCheckedValidAnswer() {
    return (checkedValidAnswers.length > 0) ? checkedValidAnswers[iterator] : null;
  }

  public void setCurrentUserAnswer(Integer answerId) { userAnswers[iterator] = answerId; }
  public void setCurrentCheckedValidAnswer(Integer answerId) { checkedValidAnswers[iterator] = questions.get(iterator).isValidAnswer(answerId); }

  public Resume(Module module){
    iterator = 0;
    questions = new ArrayList<Question>();
    if (module != null) {
      // set iterator to 0 to begin the iteration
      Iterable<Question> questionIterable = module.getQuestions();

      int counter = 0;
      for (Question question : questionIterable) {
        counter++;
        question.LoadAnswers();
        questions.add(question);
      }

      // when module created load number of questions
      userAnswers = new Integer[counter];
      checkedValidAnswers = new Boolean[counter];
    }
  }


}
