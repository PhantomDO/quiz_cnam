package fr.com.cnam.quiz.controller;

import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.model.entities.*;
import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ManageData {

    private final ThemeRepository themeRepository;
    private final ModuleRepository moduleRepository;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final AssociationRepository associationRepository;


    @Autowired
    public ManageData(  ThemeRepository themeRepository,
                        ModuleRepository moduleRepository,
                        QuestionRepository questionRepository,
                        AnswerRepository answerRepository,
                        AssociationRepository associationRepository) {
        this.themeRepository = themeRepository;
        this.moduleRepository = moduleRepository;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.associationRepository = associationRepository;
    }

    @GetMapping("/init-data")
    public String initData() {
        Theme theme1 = this.themeRepository.save(new Theme("Dévelo ppement"));
        Theme theme2 = this.themeRepository.save(new Theme("Cyber-Sécurité"));

        Module module1 = this.moduleRepository.save(new Module("UTC-501", theme1));
        Module module2 = this.moduleRepository.save(new Module("UTC-502", theme1));
        Module module3 = this.moduleRepository.save(new Module("UTC-503", theme2));
        Module module4 = this.moduleRepository.save(new Module("UTC-504", theme2));

        Answer answer1 = this.answerRepository.save(new Answer("Réponse A"));
        Answer answer2 = this.answerRepository.save(new Answer("Réponse B"));
        Answer answer3 = this.answerRepository.save(new Answer("Réponse C"));
        Answer answer4 = this.answerRepository.save(new Answer("Réponse D"));

        Question question1 = this.questionRepository.save(new Question("Quel est le sens de la vie ?", module1));
        Question question2 = this.questionRepository.save(new Question("C'est une bonne situation, ça, scribe ?", module1));
        Question question3 = this.questionRepository.save(new Question("Que pensez-vous de la situation politici-économique du Wakanda ?", module1));
        Question question4 = this.questionRepository.save(new Question("Qui est venu en premier : l'oeuf ou la poule ?", module1));
        Question question5 = this.questionRepository.save(new Question("Que c'est un pancake qui est tombé dans la neige avant le 31 décembre", module1));

        this.associationRepository.save(new Association(question1, answer1, false));
        this.associationRepository.save(new Association(question1, answer2, true));
        this.associationRepository.save(new Association(question1, answer3, false));
        this.associationRepository.save(new Association(question1, answer4, false));

        this.associationRepository.save(new Association(question2, answer1, false));
        this.associationRepository.save(new Association(question2, answer2, false));
        this.associationRepository.save(new Association(question2, answer3, false));
        this.associationRepository.save(new Association(question2, answer4, true));

        this.associationRepository.save(new Association(question3, answer1, true));
        this.associationRepository.save(new Association(question3, answer2, false));
        this.associationRepository.save(new Association(question3, answer3, false));
        this.associationRepository.save(new Association(question3, answer4, false));

        this.associationRepository.save(new Association(question4, answer1, false));
        this.associationRepository.save(new Association(question4, answer2, true));
        this.associationRepository.save(new Association(question4, answer3, false));
        this.associationRepository.save(new Association(question4, answer4, false));

        this.associationRepository.save(new Association(question5, answer1, false));
        this.associationRepository.save(new Association(question5, answer2, false));
        this.associationRepository.save(new Association(question5, answer3, false));
        this.associationRepository.save(new Association(question5, answer4, true));

        return "Success to initialize data";
    }

    @GetMapping("/purge-data")
    public String purgeData() {
        this.themeRepository.deleteAll();
        return "Sucess to delete data";
    }
}
