package fr.com.cnam.quiz.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.model.Resume;
import fr.com.cnam.quiz.model.entities.Answer;
import fr.com.cnam.quiz.model.entities.Question;
import fr.com.cnam.quiz.repository.QuestionRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;

@RestController
public class QuestionController {
    public QuestionController(QuestionRepository repository) {
        this.repository = repository;
    }

    private final QuestionRepository repository;

    @GetMapping("/question")
    public void onQuestionSelected(){
        Resume resume = QuizManager.getInstance().getResume();
        if (resume != null){
            Question question = resume.getCurrentQuestion();
            System.out.println(question.toString());
            if (question != null){
                StringBuilder sb = new StringBuilder();
                for (Answer answer : question.getAnswers()) {
                    sb.append(answer.toString());
                }
                System.out.println(sb.toString());
            }
        }
    }

    @GetMapping("/question/{answerId}")
    public RedirectView onClick(@PathVariable Integer answerId){
        Resume resume = QuizManager.getInstance().getResume();
        Question question = resume.getCurrentQuestion();
        // set what the user has answered and if it's the valid one
        if (resume != null && question != null){
            // register current values gave by the user
            resume.setCurrentUserAnswer(answerId);
            resume.setCurrentCheckedValidAnswer(answerId);
            // send to front a json object of all the answer for the question
            Integer iterator = 0;
            HashMap<String, HashMap<String, Object>> answers = new HashMap<String, HashMap<String, Object>>();
            for (Answer answer : resume.getCurrentQuestion().getAnswers()) {
                // set the value to put in the map of object #STRUCT
                Integer id = answer.getId();
                String value = answer.getValue();
                Boolean isValid = resume.getCurrentQuestion().isValidAnswer(id);
                // set the map
                HashMap<String, Object> struct = new HashMap<String, Object>();
                struct.put("id", id);
                struct.put("value", value);
                struct.put("isValid", isValid);
                // put the map in the answer map with key set to the iterator of answers
                answers.put(iterator.toString(), struct);
                iterator++;
            }

            resume.goToNextQuestion();

            // convert the map in json
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String json = objectMapper.writeValueAsString(answers);
                System.out.println(json);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        return new RedirectView("/question");
    }
}
