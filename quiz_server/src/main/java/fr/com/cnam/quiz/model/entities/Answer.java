package fr.com.cnam.quiz.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "Answer")
public class Answer {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String value;

  public Integer getId() { return id; }
  public String getValue() { return value; }

  public Answer() { }

  public Answer(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return (new StringBuilder()).append("Réponse n°").append(this.id).append("Valeur : ").append(this.value).toString();
  }
}
