package fr.com.cnam.quiz.model.entities;

import fr.com.cnam.quiz.QuizManager;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Question")
public class Question {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String sentence;

  @ManyToOne
  private Module module;
  @Transient
  private List<Answer> answers;
  @Transient
  private Integer validAnswer;

  public Integer getId() { return id; }
  public List<Answer> getAnswers() { return answers; }
  public String getSentence() { return sentence; }
  public Boolean isValidAnswer(Integer answerId) { return (answerId.equals(validAnswer)); }

  public Question() {
    this.sentence = null;
    this.module = null;
    this.answers = new ArrayList<Answer>();
  }

  public Question(String sentence, Module module) {
    this.sentence = sentence;
    this.module = module;
    this.answers = new ArrayList<Answer>();
  }

  public void LoadAnswers(){
    // create hashmap and feed it with id of answers and validAnswers boolean
    Iterable<Association> associations = QuizManager.getInstance().getAssociationRepository().findAllByQuestion(this);
    // create all answers from association map
    for (Association association : associations) {
      Answer answer = QuizManager.getInstance().getAnswerRepository().findById(association.getAnswer().getId()).orElseThrow(() ->
              new NullPointerException("Configuration not found for this id :: " + association.getAnswer()));
      answers.add(answer);
      if (association.getValid().equals(true))
      {
        // when the valid answer is found set it
        validAnswer = association.getAnswer().getId();
      }
    }
    System.out.println();
  }

  @Override
  public String toString() {
    return (new StringBuilder()).append("Question N°").append(this.id).append("Valeur : ").append(this.sentence).toString();
  }
}
