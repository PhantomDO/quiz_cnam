package fr.com.cnam.quiz;

import fr.com.cnam.quiz.model.Resume;
import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.model.entities.Theme;
import fr.com.cnam.quiz.repository.AnswerRepository;
import fr.com.cnam.quiz.repository.AssociationRepository;
import fr.com.cnam.quiz.repository.QuestionRepository;

import javax.persistence.Transient;

public class QuizManager {
    private Integer userId;
    private Theme theme;
    private Module module;
    private Resume resume;

    private AssociationRepository associationRepository;
    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;

    public Integer getUserId() { return userId; }
    public Theme getTheme() { return theme; }
    public Module getModule() { return module; }
    public Resume getResume() {
        if (resume == null) {
            resume = new Resume(module);
        }
        return resume;
    }

    public void setUserId(Integer userId) { this.userId = userId; }
    public void setTheme(Theme theme) { this.theme = theme; }
    public void setModule(Module module) {
        this.module = module;
        this.resume = new Resume(module);
    }

    public void setAssociationRepository(AssociationRepository associationRepository) { this.associationRepository = associationRepository; }
    public void setQuestionRepository(QuestionRepository questionRepository) { this.questionRepository = questionRepository; }
    public void setAnswerRepository(AnswerRepository answerRepository) { this.answerRepository = answerRepository; }

    public AssociationRepository getAssociationRepository() { return associationRepository; }
    public QuestionRepository getQuestionRepository() { return questionRepository; }
    public AnswerRepository getAnswerRepository() { return answerRepository; }

    /** Constructeur privé */
    private QuizManager() {
        userId = null;
        theme= null;
        module= null;
        resume= null;
    }

    /** Holder */
    private static class QuizHolder
    {
        /** Instance unique non préinitialisée */
        private final static QuizManager instance = new QuizManager();
    }

    /** Point d'accès pour l'instance unique du singleton */
    public static QuizManager getInstance()
    {
        return QuizHolder.instance;
    }
}
