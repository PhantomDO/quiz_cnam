package fr.com.cnam.quiz.controller;

import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.model.entities.Theme;
import fr.com.cnam.quiz.repository.ThemeRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class ThemeController {
    public ThemeController(ThemeRepository repository) {
        this.repository = repository;
    }

    private final ThemeRepository repository;

    @GetMapping("/theme")
    public Iterable<Theme> onGetAll(){
        return repository.findAll();
    }

    @GetMapping("/theme/{themeId}")
    public RedirectView onSelected(@PathVariable Integer themeId){
        Theme theme = repository.findById(themeId).orElseThrow(() -> new NullPointerException("Configuration not found for this id :: " + themeId));
        QuizManager.getInstance().setTheme(theme);
        System.out.println(QuizManager.getInstance().getTheme());
        return new RedirectView("/module");
    }
}
