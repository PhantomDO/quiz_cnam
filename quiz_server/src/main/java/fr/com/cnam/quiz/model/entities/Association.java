package fr.com.cnam.quiz.model.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Association")
public class Association implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private Question question;
    @ManyToOne
    private Answer answer;
    private Boolean isValid;

    public Integer getId() { return id; }
    public Question getQuestion() { return question; }
    public Answer getAnswer() { return answer; }
    public Boolean getValid() { return isValid; }

    public Association() {
        this.question = null;
        this.answer = null;
        this.isValid = null;}

    public Association(Question question, Answer answer, Boolean isValid) {
        this.question = question;
        this.answer = answer;
        this.isValid = isValid;
    }
}
