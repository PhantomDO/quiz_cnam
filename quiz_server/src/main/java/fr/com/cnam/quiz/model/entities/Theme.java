package fr.com.cnam.quiz.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "Theme")
public class Theme {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String name;

  public Theme() {}
  public Theme(String name) {
    this.name = name;
  }

  public Integer getId() { return id; }
  public String getName() { return name; }
  public void setId(Integer id) {
    this.id = id;
  }
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return (new StringBuilder()).append("Thème n° ").append(this.id).append("Nom : ").append(this.name).toString();
  }
}