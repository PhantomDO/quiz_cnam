package fr.com.cnam.quiz.repository;

import fr.com.cnam.quiz.model.entities.Module;
import fr.com.cnam.quiz.model.entities.Theme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThemeRepository extends CrudRepository<Theme, Integer> {

}
