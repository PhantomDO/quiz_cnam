package fr.com.cnam.quiz.model.entities;

import fr.com.cnam.quiz.QuizManager;
import fr.com.cnam.quiz.repository.AnswerRepository;
import fr.com.cnam.quiz.repository.AssociationRepository;
import fr.com.cnam.quiz.repository.QuestionRepository;

import javax.persistence.*;

@Entity
@Table(name = "Module")
public class Module {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String name;

  @ManyToOne
  private Theme theme;


  public Integer getId() { return id; }
  public String getName() { return name; }
  public Theme getTheme() { return theme; }



  public Module() { }

  public Module(String name, Theme theme){
    this.name = name;
    this.theme = theme;
  }

  public Module(String name, Theme theme, AssociationRepository associationRepository, QuestionRepository questionRepository, AnswerRepository answerRepository) {
    this.name = name;
    this.theme = theme;
  }

  public Iterable<Question> getQuestions(){
    Iterable<Question> questions = QuizManager.getInstance().getQuestionRepository().findQuestionsByModule(this);
    return questions;
  }

  @Override
  public String toString() {
    return (new StringBuilder()).append("Module N° ").append(this.id).append("Nom : ").append(this.name).toString();
  }
}
