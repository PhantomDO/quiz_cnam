package fr.com.cnam.quiz.model.entities;

import javax.persistence.*;

@Entity
@Table(name="User")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String name;
  private String lastname;
  private String login;
  private String password;
  private Integer role;

  public Integer getId() { return id; }
  public String getName() { return name; }
  public String getLastname() { return lastname; }
  public String getLogin() { return login; }
  public String getPassword() { return password; }
  public Integer getRole() { return role; }

  public User() { }

  public User(Integer id) {
    this.id = id;
  }

}
