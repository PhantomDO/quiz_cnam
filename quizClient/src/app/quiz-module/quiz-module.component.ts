import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  *  as  data  from  '../../assets/data/data.json';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-quiz-module',
  templateUrl: './quiz-module.component.html',
  styleUrls: ['./quiz-module.component.scss']
})
export class QuizModuleComponent implements OnInit {
  dataSimulate = (data  as  any).default;
  public idTheme = 0;
  public themeName: string | undefined;

  constructor(private route: ActivatedRoute,
              private api: ApiService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.idTheme = params.id;
    });

    this.getThemeNaleById();
    this.getModuleByTheme();
  }

  getThemeNaleById(): any {
    this.api.getAllTheme().subscribe((data: any)=> {
      let themeData = [];
      themeData.push(data);
      this.themeName = themeData[0][this.idTheme -1].name;
    })
  }

  getModuleByTheme(): any {
    return this.dataSimulate.theme[this.idTheme - 1].module;

  }

}
