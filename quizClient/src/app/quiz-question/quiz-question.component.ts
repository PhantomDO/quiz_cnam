import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quiz-question',
  templateUrl: './quiz-question.component.html',
  styleUrls: ['./quiz-question.component.scss']
})
export class QuizQuestionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public confirmResponse() {
      confirm(`do you confirm your response? `);
  }

}
