import { Component, OnInit } from '@angular/core';
import  *  as  data  from  '../../assets/data/data.json';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public themeData: any;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getAllTheme();
  }

  private getAllTheme() {
    this.api.getAllTheme().subscribe(data=> {
      this.themeData = data;
    })
  }
}
