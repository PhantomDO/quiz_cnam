import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { QuizModuleComponent } from './quiz-module/quiz-module.component';
import { QuizQuestionComponent } from './quiz-question/quiz-question.component';
import { AuthGuardService } from './service/auth-guard.service';

const routes: Routes = [
  //{ path: '', canActivate: [AuthGuardService], component:  HomeComponent},
  { path: '', component:  HomeComponent},
  { path: 'login',component:  LoginComponent},
  { path: 'modules/:id',component:  QuizModuleComponent},
  { path: 'modules/:id/question/:id',component:  QuizQuestionComponent},
  { path: 'profile',component:  ProfileComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
