import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private localHost = 'http://localhost:8080';
  private host = 'http://192.168.12.94:8080';

  constructor(private http: HttpClient) { }

  public getAllTheme() {
    return this.http.get(`${this.host}/theme`);
  }

  public getModuleByTheme() {
    return this.http.get(`${this.host}/module`);
  }
}
